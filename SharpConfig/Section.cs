﻿/*
 * Copyright (c) 2013-2014 Cemalettin Dervis
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Text;

namespace SharpConfig
{
    /// <summary>
    /// Represents a group of <see cref="Setting"/> objects.
    /// </summary>
    public sealed class Section : ConfigurationElement
    {
        private SettingCollection mSettings;

        /// <summary>
        /// Initializes a new instance of the <see cref="Section"/> class.
        /// </summary>
        public Section( )
            : this(string.Empty)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="Section"/> class.
        /// </summary>
        ///
        /// <param name="name">The name of the section.</param>
        public Section( string name )
            : base( name, null )
        {
            mSettings = new SettingCollection();
        }

        /// <summary>
        /// Creates an object of a specific type, and maps the settings
        /// in this section to the public properties of the object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>The created object.</returns>
        /// 
        /// <remarks>
        /// The specified type must have a public default constructor
        /// in order to be created.
        /// </remarks>
        public T CreateObject<T>() where T : class
        {
            Type type = typeof( T );

            try
            {
                T obj = Activator.CreateInstance<T>();

                MapTo( obj, false );

                return obj;
            }
            catch ( Exception )
            {
                throw new ArgumentException( string.Format(
                    "The type '{0}' does not have a default public constructor.",
                    type.Name ) );
            }
        }

        /// <summary>
        /// Assigns the values of this section to an object's public properties.
        /// </summary>
        ///
        /// <typeparam name="T">Generic type parameter.</typeparam>
        /// 
        /// <param name="obj">       The object.</param>
        public void MapTo<T>( T obj, bool ignoreCase ) where T : class
        {
            if ( obj == null )
                throw new ArgumentNullException( "obj" );

            Type type = typeof( T );

            var properties = type.GetProperties();

            foreach ( var prop in properties )
            {
                if ( !prop.CanWrite )
                    continue;

                var setting = mSettings[prop.Name, ignoreCase];

                if (setting != null)
                {
                    object value = setting.GetValue( prop.PropertyType );

                    prop.SetValue( obj, value, null );
                }
            }
        }

        /// <summary>
        /// Gets the settings of this section.
        /// </summary>
        public SettingCollection Settings
        {
            get { return mSettings; }
        }

        /// <summary>
        /// Gets a setting by its name.
        /// </summary>
        ///
        /// <param name="name">The name of the setting.</param>
        ///
        /// <returns>
        /// The setting if found, null otherwise.
        /// </returns>
        public Setting this[string name]
        {
            get { return mSettings[name, !Configuration.IsCaseSensitive]; }
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        ///
        /// <returns>
        /// A string that represents the current object.
        /// </returns>
        public override string ToString()
        {
            return ToString( false );
        }

        /// <summary>
        /// Convert this object into a string representation.
        /// </summary>
        ///
        /// <param name="includeComment">True to include, false to exclude the comment.</param>
        ///
        /// <returns>
        /// A string that represents the current object.
        /// </returns>
        public string ToString( bool includeComment )
        {
            if ( includeComment && Comment != null )
                return string.Format( "[{0}] {1}", Name, Comment.ToString() );
            else
                return string.Format( "[{0}]", Name );
        }
    }
}