﻿/*
 * Copyright (c) 2013-2014 Cemalettin Dervis
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace SharpConfig
{
    /// <summary>
    /// Represents a collection of <see cref="Section"/> objects.
    /// </summary>
    public sealed class SectionCollection : Collection<Section>
    {
        internal SectionCollection()
        { }

        /// <summary>
        /// Gets a section by its name using case-sensitive search.
        /// </summary>
        ///
        /// <param name="name">The name of the section.</param>
        ///
        /// <returns>
        /// The section if found, null otherwise.
        /// </returns>
        public Section this[string name]
        {
            get { return this[name, false]; }
        }

        /// <summary>
        /// Gets a section by its name.
        /// </summary>
        ///
        /// <param name="name">      The name of the section.</param>
        /// <param name="ignoreCase">True to ignore case sensitivity, false otherwise.</param>
        ///
        /// <returns>
        /// The section if found, null otherwise.
        /// </returns>
        public Section this[string name, bool ignoreCase]
        {
            get
            {
                var strCmp = ignoreCase ?
                    StringComparison.InvariantCultureIgnoreCase : StringComparison.Ordinal;

                foreach ( var section in this )
                {
                    if ( string.Equals( section.Name, name, strCmp ) )
                        return section;
                }

                return null;
            }
        }

    }
}