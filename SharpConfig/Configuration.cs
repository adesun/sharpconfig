﻿/*
 * Copyright (c) 2013-2014 Cemalettin Dervis
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace SharpConfig
{
    /// <summary>
    /// Represents a configuration.
    /// Configurations contain one or multiple sections
    /// that in turn can contain one or multiple settings.
    /// The <see cref="Configuration"/> class is designed
    /// to work with classic configuration formats such as
    /// .ini and .cfg, but is not limited to these.
    /// </summary>
    public partial class Configuration
    {
        #region Fields

        /// <summary>
        /// Name of the global section.
        /// The default value is "General".
        /// </summary>
        public const string GlobalSectionName = "General";

        private static NumberFormatInfo mNumberFormat;
        private static bool mIsCaseSensitive;
        private static char[] mValidCommentChars;

        private SectionCollection mSections;

        #endregion

        #region Construction

        static Configuration()
        {
            mNumberFormat = CultureInfo.InvariantCulture.NumberFormat;
            mIsCaseSensitive = true;
            mValidCommentChars = new[] { '#', ';', '\'' };
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Configuration"/> class.
        /// </summary>
        public Configuration()
        {
            mSections = new SectionCollection();
            mSections.Add( new Section( Configuration.GlobalSectionName ) );
        }

        #endregion

        #region Load

        /// <summary>
        /// Loads a configuration from a file auto-detecting the encoding and
        /// using the default parsing settings.
        /// </summary>
        ///
        /// <param name="filename">The location of the configuration file.</param>
        ///
        /// <returns>
        /// The loaded <see cref="Configuration"/> object.
        /// </returns>
        public static Configuration LoadFromFile( string filename )
        {
            return LoadFromFile( filename, null );
        }

        /// <summary>
        /// Loads a configuration from a file.
        /// </summary>
        ///
        /// <param name="filename">The location of the configuration file.</param>
        /// <param name="encoding">The encoding applied to the contents of the file. Specify null to auto-detect the encoding.</param>
        ///
        /// <returns>
        /// The loaded <see cref="Configuration"/> object.
        /// </returns>
        public static Configuration LoadFromFile( string filename, Encoding encoding )
        {
            if ( !File.Exists( filename ) )
                throw new FileNotFoundException( "Configuration file not found.", filename );

            Configuration cfg = null;

            if ( encoding == null )
                cfg = LoadFromText( File.ReadAllText( filename ) );
            else
                cfg = LoadFromText( File.ReadAllText( filename, encoding ) );

            return cfg;
        }

        /// <summary>
        /// Loads a configuration from a text stream auto-detecting the encoding and
        /// using the default parsing settings.
        /// </summary>
        ///
        /// <param name="stream">The text stream to load the configuration from.</param>
        ///
        /// <returns>
        /// The loaded <see cref="Configuration"/> object.
        /// </returns>
        public static Configuration LoadFromStream( Stream stream )
        {
            return LoadFromStream( stream, null );
        }

        /// <summary>
        /// Loads a configuration from a text stream.
        /// </summary>
        ///
        /// <param name="stream">   The text stream to load the configuration from.</param>
        /// <param name="encoding"> The encoding applied to the contents of the stream. Specify null to auto-detect the encoding.</param>
        ///
        /// <returns>
        /// The loaded <see cref="Configuration"/> object.
        /// </returns>
        public static Configuration LoadFromStream( Stream stream, Encoding encoding )
        {
            if ( stream == null )
                throw new ArgumentNullException( "stream" );

            string source = null;

            var reader = encoding == null ?
                new StreamReader( stream ) : new StreamReader( stream, encoding );

            using ( reader )
            {
                source = reader.ReadToEnd();
                reader.Close();
            }

            return LoadFromText( source );
        }

        /// <summary>
        /// Loads a configuration from text (source code).
        /// </summary>
        ///
        /// <param name="source">   The text (source code) of the configuration.</param>
        ///
        /// <returns>
        /// The loaded <see cref="Configuration"/> object.
        /// </returns>
        public static Configuration LoadFromText( string source )
        {
            if ( string.IsNullOrEmpty( source ) )
                throw new ArgumentNullException( "source" );

            return Parse( source );
        }

        #endregion

        #region LoadBinary

        /// <summary>
        /// Loads a configuration from a binary file using a specific reader.
        /// </summary>
        ///
        /// <param name="reader">  The reader to use. Specify null to use the default reader.</param>
        /// <param name="filename">The location of the configuration file.</param>
        ///
        /// <returns>
        /// The configuration.
        /// </returns>
        public static Configuration LoadBinary( BinaryReader reader, string filename )
        {
            if ( string.IsNullOrEmpty( filename ) )
                throw new ArgumentNullException( "filename" );
            
            return DeserializeBinary( reader, filename );
        }

        /// <summary>
        /// Loads a configuration from a binary stream, using a specific <see cref="BinaryReader"/>.
        /// </summary>
        ///
        /// <param name="reader">The reader to use. Specify null to use the default reader.</param>
        /// <param name="stream">The stream to load the configuration from.</param>
        ///
        /// <returns>
        /// The configuration.
        /// </returns>
        public static Configuration LoadBinary( BinaryReader reader, Stream stream )
        {
            if ( stream == null )
                throw new ArgumentNullException( "stream" );

            return DeserializeBinary( reader, stream );
        }

        #endregion

        #region Save

        /// <summary>
        /// Saves the configuration to a file using the default character encoding, which is UTF8.
        /// </summary>
        ///
        /// <param name="filename">The location of the configuration file.</param>
        public void Save( string filename )
        {
            Save( filename, null );
        }

        /// <summary>
        /// Saves the configuration to a file.
        /// </summary>
        ///
        /// <param name="filename">The location of the configuration file.</param>
        /// <param name="encoding">The character encoding to use. Specify null to use the default encoding, which is UTF8.</param>
        public void Save( string filename, Encoding encoding )
        {
            if ( string.IsNullOrEmpty( filename ) )
                throw new ArgumentNullException( "filename" );

            Serialize( filename, encoding );
        }

        /// <summary>
        /// Saves the configuration to a stream using the default character encoding, which is UTF8.
        /// </summary>
        ///
        /// <param name="stream">The stream to save the configuration to.</param>
        public void Save( Stream stream )
        {
            Save( stream, null );
        }

        /// <summary>
        /// Saves the configuration to a stream.
        /// </summary>
        ///
        /// <param name="stream">The stream to save the configuration to.</param>
        /// <param name="encoding">The character encoding to use. Specify null to use the default encoding, which is UTF8.</param>
        public void Save( Stream stream, Encoding encoding )
        {
            if ( stream == null )
                throw new ArgumentNullException( "stream" );

            Serialize( stream, encoding );
        }

        #endregion

        #region SaveBinary

        /// <summary>
        /// Saves the configuration to a binary file, using the default <see cref="BinaryWriter"/>.
        /// </summary>
        ///
        /// <param name="filename">The location of the configuration file.</param>
        public void SaveBinary( string filename )
        {
            SaveBinary( filename, null );
        }

        /// <summary>
        /// Saves the configuration to a binary file, using a specific <see cref="BinaryWriter"/>.
        /// </summary>
        ///
        /// <param name="filename">The location of the configuration file.</param>
        /// <param name="writer">  The writer to use. Specify null to use the default writer.</param>
        public void SaveBinary( string filename, BinaryWriter writer )
        {
            if ( string.IsNullOrEmpty( filename ) )
                throw new ArgumentNullException( "filename" );

            SerializeBinary( writer, filename );
        }

        /// <summary>
        /// Saves the configuration to a binary stream, using the default <see cref="BinaryWriter"/>.
        /// </summary>
        ///
        /// <param name="stream">The stream to save the configuration to.</param>
        public void SaveBinary( Stream stream )
        {
            SaveBinary( stream, null );
        }

        /// <summary>
        /// Saves the configuration to a binary file, using a specific <see cref="BinaryWriter"/>.
        /// </summary>
        ///
        /// <param name="stream">The stream to save the configuration to.</param>
        /// <param name="writer">The writer to use. Specify null to use the default writer.</param>
        public void SaveBinary( Stream stream, BinaryWriter writer )
        {
            if ( stream == null )
                throw new ArgumentNullException( "stream" );

            SerializeBinary( writer, stream );
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the number format that is used for value conversion in <see cref="Setting.GetValue()"/>.
        /// The default value is <b>CultureInfo.InvariantCulture.NumberFormat</b>.
        /// </summary>
        public static NumberFormatInfo NumberFormat
        {
            get { return mNumberFormat; }
            set
            {
                if ( value == null )
                    throw new ArgumentNullException( "value" );

                mNumberFormat = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether configurations
        /// perform a case-sensitive search when searching for sections and settings.
        /// </summary>
        public static bool IsCaseSensitive
        {
            get { return mIsCaseSensitive; }
            set { mIsCaseSensitive = value; }
        }

        /// <summary>
        /// Gets or sets the array that contains all comment delimiting characters.
        /// </summary>
        public static char[] ValidCommentChars
        {
            get { return mValidCommentChars; }
            set
            {
                if ( value == null )
                    throw new ArgumentNullException( "value" );

                if ( value.Length == 0 )
                {
                    throw new ArgumentException(
                        "The comment chars array must not be empty.",
                        "value" );
                }

                mValidCommentChars = value;
            }
        }

        /// <summary>
        /// Gets the global section of this configuration.
        /// </summary>
        public Section GlobalSection
        {
            get { return mSections[GlobalSectionName]; }
        }

        /// <summary>
        /// Gets the sections of this configuration.
        /// </summary>
        public SectionCollection Sections
        {
            get { return mSections; }
        }

        /// <summary>
        /// Gets a section by its name.
        /// </summary>
        ///
        /// <param name="name">The name of the section.</param>
        ///
        /// <returns>
        /// The section if found, null otherwise.
        /// </returns>
        public Section this[string name]
        {
            get { return mSections[name, !mIsCaseSensitive]; }
        }

        #endregion
    }
}