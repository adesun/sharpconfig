![sharpconfig_logo.png](https://bitbucket.org/repo/ER5Ajb/images/1435446224-sharpconfig_logo.png)

SharpConfig is an easy-to-use CFG/INI configuration library for .NET.

You can use SharpConfig in your .NET applications to add the functionality
to read, modify and save configuration files and streams, in either text or binary format.

A configuration file example:
```cfg
[General]
# a comment
SomeString = Hello World!
SomeInteger = 10 # an inline comment
SomeFloat = 20.05
ABoolean = true
```

To read these values, your C# code would look like:
```csharp
Configuration config = Configuration.LoadFromFile("sample.cfg");
Section section = config["General"];

string someString = section["SomeString"].Value;
int someInteger = section["SomeInteger"].GetValue<int>();
float someFloat = section["SomeFloat"].GetValue<float>();
```

Enumerations
---

SharpConfig is also able to parse enumerations.
For example you have a configuration like this:
```cfg
[DateInfo]
Day = Monday
```

It is now possible to read this value as a System.DayOfWeek enum, because Monday is present there.
An example of how to read it:

```csharp
DayOfWeek day = config["DateInfo"]["Day"].GetValue<DayOfWeek>();
```

Arrays
---

Arrays are also supported in SharpConfig.
For example you have a configuration like this:
```cfg
[General]
MyArray = [0,2,5,6]
```

This array can be interpreted as any type array that can be converted from 0, 2, 5 and 6, for example int, float, double, char, byte, string etc.

Reading this array is simple:
```csharp
Section section = config["General"];
Setting setting = section["MyArray"];

int[] intArray = setting.GetValueArray<int>();
float[] floatArray = setting.GetValueArray<float>();
string[] stringArray = setting.GetValueArray<string>();
```

Assignments
---

A special feature is the assignment of categories and properties.
Imagine you have a structure and enumeration in C# like this:
```csharp
struct PersonInfo
{
    public string Name { get; set; }
    public int Age { get; set; }
    public Gender Gender { get; set; }
}

enum Gender
{
    Male,
    Female
}
```

It is possible to read the entire object straight from the configuration file:
```cfg
[Person]
Name = Peter
Age = 50
Gender = Male
```
Like this:
```csharp
PersonInfo person = new PersonInfo();
person = config["Person"].AssignTo(person);
```

Note: The assignment only works on public properties and primitive data types (int, bool, enums ...).

Installing via NuGet
---

You can install SharpConfig via the following NuGet command:
> Install-Package sharpconfig